const express = require('express');
const bodyParser = require('body-parser');
const pdf = require('html-pdf');
const cors = require('cors');
const pdfTemplate = require('./documents');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const PDF_FILE = `report.pdf`;

app.get('/create-pdf', (req, res) => {
    const object = {
        name: 'hola',
        price1: 211,
        price2: 334,
        receiptId: '3u3u'
    }
    pdf.create(pdfTemplate(object), {}).toFile(`${PDF_FILE}`, (err) => {
        if (err) {
            return console.log('error');
        }

        console.log("Fichero creado y se abrirá en el navegador");
        // Abrir directamente el pdf generado 
        res.sendFile(`${__dirname}/${PDF_FILE}`);
    });
})

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
    console.log('Para crear el PDF haz click aquí: http://localhost:5000/create-pdf');
});